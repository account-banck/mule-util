const express = require('express');
const {createProxyMiddleware} = require('http-proxy-middleware');
const cors = require('cors');
const app = express();

const port = 3000;
// Utilisez cors pour toutes les routes
app.use(cors());

app.use('/api', createProxyMiddleware({
  target: 'https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/f1e97bc6-315a-4490-82a7-23abe036327a.anypoint-platform/amc-application-manager/4.0.4/m',
  changeOrigin: true,
  pathRewrite: {
    '^/api': '', // remove base path
  }, on: {

    proxyReq: (proxyReq, req, res) => {
      console.log(`Requête envoyée au proxy: ${req.url}`);
    },
    proxyRes: (proxyRes, req, res) => {
      console.log(`Réponse reçue du proxy pour la requête ${req.url} avec le code de statut ${proxyRes.statusCode}`);
    },
    error: (err, req, res) => {
      console.error('Erreur de proxy:', err);
      res.status(500).send('Erreur de proxy');
    },
  }
}));

app.listen(port, () => {
  console.log(`Serveur en cours d'exécution à http://localhost:${port}`);
});
