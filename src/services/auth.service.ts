// auth.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string;

  constructor() {
    // Charger le token depuis le stockage local ou initialiser
    this.token = localStorage.getItem('authToken') || '';
  }

  getToken(): string {
    return this.token;
  }

  setToken(token: string): void {
    this.token = token;
    localStorage.setItem('authToken', token);
  }

  clearToken(): void {
    this.token = '';
    localStorage.removeItem('authToken');
  }
}
