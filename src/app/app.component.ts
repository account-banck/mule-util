import {Component, OnDestroy, OnInit} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppService } from "../services/app.service";
import AppDetail from "../models/AppDetail";
import {lastValueFrom, Subscription} from "rxjs";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";

@Component({
  selector: 'app-root',
  standalone: true,
   imports: [
     MatButtonModule,
     MatTableModule,
     MatPaginatorModule,
     MatSortModule,
     MatButtonModule,
     CommonModule,
     RouterOutlet],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Mule-Utils';
  detail: AppDetail[] = [];
subscription= new Subscription();
  constructor(private appService: AppService) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    // this.subscription.unsubscribe();
    this.subscription.add( this.appService.getAppByOrganizationAndEnvironment('e4899eda-22c9-4546-8ad2-ffde5a3d466d', 'e4899eda-22c9-4546-8ad2-ffde5a3d466d').subscribe((apps) => {
      this.detail = []; // Réinitialiser la liste des détails
      apps.items.forEach((app) => {
        lastValueFrom(this.appService.getAppByOrganizationAndEnvironmentAndDeplymentId('e4899eda-22c9-4546-8ad2-ffde5a3d466d', 'e4899eda-22c9-4546-8ad2-ffde5a3d466d', app.id)).then((appDetail) => {

          this.detail.push(appDetail);
        });
      });
    }));
  }


  refreshData() {
    this.loadData();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
