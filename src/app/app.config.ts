import {APP_INITIALIZER, ApplicationConfig} from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';
import {AuthService} from "../services/auth.service";
import {HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {ConfigService} from "../services/config.service";
import {AuthInterceptor} from "../interceptor/auth.interceptor";

export function initializeApp(configService: ConfigService) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      configService.loadConfig().subscribe({
        next: (config) => {
          configService.setConfig(config);
          resolve(true);
        },
        error: (error) => {
          reject(error);
        }
      });
    });
  };
}

export const appConfig: ApplicationConfig = {
  providers: [
    provideHttpClient(withInterceptorsFromDi()),  {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    provideRouter(routes),
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ConfigService],
      multi: true
    },

    AuthService,
    ConfigService
  ]
};
